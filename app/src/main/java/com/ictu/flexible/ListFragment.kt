package com.ictu.flexible

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.item_list_view.view.*

class ListFragment : Fragment() {

    private val data = ArrayList<String>()

    private val adapter = object : BaseAdapter() {
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val inflater = requireContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val view = inflater.inflate(R.layout.item_list_view, null)

            view.text_item.text = data[position]
            return view
        }

        override fun getItem(position: Int): Any {
            return position
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return data.size
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        data.add("Viet Nam")
        data.add("Han Quoc")
        data.add("Trung Quoc")
        data.add("Thai Lan")
        data.add("Campuchia")
        data.add("My")
        data.add("An Do")
        data.add("Duc")
        data.add("Phap")
        data.add("Peru")
        data.add("Mexico")
        data.add("Lao")
        data.add("Nhat Ban")

        list_view.adapter = adapter

        adapter.notifyDataSetChanged()

        list_view.setOnItemClickListener { parent, view, position, id ->
            val value = data[position]
            (requireActivity() as MainActivity).onFragmentListener.onListener(value)
        }

    }
}
