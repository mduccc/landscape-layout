package com.ictu.flexible

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.fragment_home.*


class MainActivity : AppCompatActivity() {

    val onFragmentListener = object : OnFragmentListener {
        override fun onListener(value: String) {
            supportFragmentManager.findFragmentById(R.id.home_frag).let {
                if (it != null && it.isInLayout)
                    it.home_title.text = value
                else {
                    val intent = Intent(this@MainActivity, HomeActivity::class.java)
                    intent.putExtra("home_title", value)
                    startActivity(intent)
                }

            }
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

}
