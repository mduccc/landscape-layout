package com.ictu.flexible

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.fragment_home.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val value = intent.getStringExtra("home_title")

        value?.let {
            supportFragmentManager.findFragmentById(R.id.home_frag)?.let {
                it.home_title.text = value
            }
        }
    }

    override fun onPause() {
        super.onPause()
        finish()
    }
}
